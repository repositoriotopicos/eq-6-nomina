
package mvcnomina;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import mvc.Modelo;
import mvc.Vista;


public class Controlador implements ActionListener {
    
    private Modelo modelo;
    private Vista vista;
    private Double cantidad;
    
    public Controlador( Modelo modelo, Vista vista) {
        this.modelo = modelo;
        this.vista = vista;
  
        this.vista.salir.addActionListener(this);
        this.vista.aceptar.addActionListener(this);
        this.vista.cancelar.addActionListener(this);
        this.vista.combo.addActionListener(this);
        
    }
    public void iniciarVista() {
        vista.setTitle("Control De Nomina");
        vista.pack();
        vista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
       
        
    }
public void almacenarDatos(){
        
        String datos[]= new String [5];
        
        if (vista.texto1.getText().length()==0){
            JOptionPane.showMessageDialog(null, "ingresa el Nombre");
            return;
        }
        
        if (vista.t2.getText().length()==0){
            JOptionPane.showMessageDialog(null, "Ingresa el apellido");
            return;
        }
        if (vista.t3.getText().length()==0){
            JOptionPane.showMessageDialog (null, "Ingrese la clave");
            return;
        }
        if (vista.t4.getText().length()==0){
            JOptionPane.showMessageDialog(null, "Ingrese el seguro ");
            return;
            
        }
        datos [0]= vista.texto1.getText();
        datos [1]= vista.t2.getText();
        datos [2]= vista.t3.getText();
        datos [3]= vista.t4.getText();
         
        System.out.println("  Datos " + datos[1] + datos[0]);
    }
            
    
    public void limpiarTextos(){
        
        vista.texto1.setText("  ");
        vista.t2.setText("  ");
        vista.t3.setText("  ");
        vista.t4.setText("  ");
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
   
        if (vista.aceptar == e.getSource()){
            almacenarDatos();
            limpiarTextos();
            
            
        }
        
        if (vista.cancelar==e.getSource()){
            limpiarTextos();
        }
        
        if (vista.salir== e.getSource()){
            System.exit(0);
        }
    
    
    }
    
    
    
}
