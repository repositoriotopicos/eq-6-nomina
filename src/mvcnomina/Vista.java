package mvcnomina;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class Vista extends JFrame {
 public JButton salir, aceptar, cancelar;
    public JComboBox combo;
    public JPanel panelNorte, panelCentro, panelSur;
    public JLabel etiqueta1, e2, e3, e4, e5, e6;
    public JTextField texto1, t2, t3, t4;
    
    public Vista(){
      getContentPane().setLayout(new BorderLayout());
       panelNorte = new JPanel();
       panelNorte.setLayout(new FlowLayout());
       etiqueta1= new JLabel("Tipo de Trabajador");
       combo= new JComboBox();
       combo.addItem("Asalariado");
       combo.addItem("Sueldo por hora");
       combo.addItem("Sueldo por comision");
       combo.addItem ("Sueldo base mas comision");
       panelNorte.add(etiqueta1);
       panelNorte.add(combo);
       
       panelCentro = new JPanel();
       panelCentro.setLayout(new BorderLayout());
       JPanel dentroCentro = new JPanel();
       dentroCentro.setLayout(new GridLayout(2,2));
       e2= new JLabel("Nombre ");
       e3= new JLabel ("Apellidos ");
       texto1= new JTextField(15);
       t2= new JTextField (15);
       
       dentroCentro.add(e2);
       dentroCentro.add(texto1);
       dentroCentro.add(e3);
       dentroCentro.add(t2);
      // dentroCentro.add( e6= new JLabel ("Datos del Trabajador"));
       panelCentro.add(dentroCentro, BorderLayout.NORTH);
       
               
       JPanel dentroSur = new JPanel();
       dentroSur.setLayout(new GridLayout(2,2));
       e4 = new JLabel("Clave ");
       t3= new JTextField(15);
       e5= new JLabel ("Numero del Seguro Social");
       t4= new JTextField (10);
       dentroSur.add(e4);
       dentroSur.add(t3);
       dentroSur.add(e5);
       dentroSur.add(t4);
       panelCentro.add(dentroSur, BorderLayout.SOUTH);
       
       panelSur = new JPanel();
       panelSur.setLayout(new FlowLayout());
       salir = new JButton ("Salir");
       aceptar= new JButton ("Aceptar");
       cancelar= new JButton ( "Cancelar");
       
       panelSur.add(aceptar);
       panelSur.add(cancelar);
       panelSur.add(salir);
       
       add(panelSur, BorderLayout.SOUTH);
        add(panelNorte, BorderLayout.NORTH);
       add(panelCentro, BorderLayout.CENTER);
               
                }
}
